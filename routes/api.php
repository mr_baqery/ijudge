<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChallengeController;
use App\Http\Controllers\EmailPolicyController;
use App\Http\Controllers\ForgetPasswordController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\SubmissionController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\UserEmailController;
use App\Http\Controllers\UserProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix("auth")->group(function () {
    Route::post("/login", [AuthController::class, 'login'])->name("auth.login");
    Route::get("/logout", [AuthController::class, 'logout'])->name("auth.logout");
    Route::get("/refresh", [AuthController::class, 'refresh'])->name("auth.refresh");
    Route::get("/me", [AuthController::class, 'me'])->name("auth.me");
    Route::post("/register", [RegistrationController::class, 'register'])->name("auth.register");
    Route::get("/verify/{id}", [RegistrationController::class, 'verify'])->name("verification.verify");
    Route::get("/resend", [RegistrationController::class, 'resend'])->name('auth.resend');
    Route::post("/forget_password", [ForgetPasswordController::class, 'forget'])->name('auth.forget');
    Route::post("/reset_password", [ForgetPasswordController::class, 'reset'])->name('password.reset');
//    Route::post("/change_password", ) @Todo
});

Route::post("/email_policies", [EmailPolicyController::class, 'update'])->name("email_policies.update");
Route::get("/email_policies", [EmailPolicyController::class, 'show'])->name("email_policies.show");

Route::get("/user_profile", [UserProfileController::class, 'show'])->name("user_profile.show");
Route::post("/user_profile", [UserProfileController::class, 'update'])->name("user_profile.update");

Route::get("/user_email", [UserEmailController::class, 'index'])->name("user_email.index");
Route::post("/user_email", [UserEmailController::class, 'store'])->name("user_email.store");
Route::get("/user_email/{userEmail}", [UserEmailController::class, 'show'])->name("user_email.show");
Route::put("/user_email/{userEmail}", [UserEmailController::class, 'update'])->name("user_email.update");
Route::post("/active_email", [UserEmailController::class, 'changeActiveEmail'])->name("user_email.active_email");

Route::get("/tests", [TestController::class, 'index'])->name("tests.index");
Route::post("/tests", [TestController::class, 'store'])->name("tests.create");
Route::put("/test/{test}", [TestController::class, 'update'])->name("tests.update");
Route::delete("/test/{test}", [TestController::class, 'destroy'])->name('tests.destroy');

Route::post("/test/{test}/challenges", [ChallengeController::class, "store"])->name("challenge.store");
Route::put("/test/{test}/challenge", [ChallengeController::class, "update"])->name("challenge.store");

Route::get("/tests/{test}/submissions", [SubmissionController::class, "index"])->name("submissions.index");
Route::post("/tests/{test}/submissions", [SubmissionController::class, "store"])->name("submissions.store");
Route::get("/tests/{test}/submissions/{submission}", [SubmissionController::class, 'show'])->name("submission.show");
Route::delete("/submissions/{submission}", [SubmissionController::class, 'destroy'])->name("submissions.destroy");

