<?php

namespace App\Admin\Controllers;

use App\Models\Challenge;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ChallengeController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Challenge';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Challenge());

        $grid->column('id', __('Id'));
        $grid->column('test_id', __('Test id'));
        $grid->column('title', __('Title'));
        $grid->column('point', __('Point'));
        $grid->column('description', __('Description'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Challenge::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('test_id', __('Test id'));
        $show->field('title', __('Title'));
        $show->field('point', __('Point'));
        $show->field('description', __('Description'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Challenge());

        $form->number('test_id', __('Test id'));
        $form->text('title', __('Title'));
        $form->number('point', __('Point'));
        $form->textarea('description', __('Description'));

        return $form;
    }
}
