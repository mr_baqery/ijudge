<?php

namespace App\Admin\Controllers;

use App\Models\UserProfile;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class UserProfileController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'UserProfile';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new UserProfile());

        $grid->column('id', __('Id'));
        $grid->column('user_id', __('User id'));
        $grid->column('total_point', __('Total point'));
        $grid->column('profile_pic', __('Profile pic'));
        $grid->column('university', __('University'));
        $grid->column('state', __('State'));
        $grid->column('phone_no', __('Phone no'));
        $grid->column('private_profile', __('Private profile'));
        $grid->column('username', __('Username'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(UserProfile::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('user_id', __('User id'));
        $show->field('total_point', __('Total point'));
        $show->field('profile_pic', __('Profile pic'));
        $show->field('university', __('University'));
        $show->field('state', __('State'));
        $show->field('phone_no', __('Phone no'));
        $show->field('private_profile', __('Private profile'));
        $show->field('username', __('Username'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new UserProfile());

        $form->number('user_id', __('User id'));
        $form->number('total_point', __('Total point'));
        $form->text('profile_pic', __('Profile pic'));
        $form->text('university', __('University'));
        $form->text('state', __('State'));
        $form->text('phone_no', __('Phone no'));
        $form->switch('private_profile', __('Private profile'));
        $form->text('username', __('Username'));

        return $form;
    }
}
