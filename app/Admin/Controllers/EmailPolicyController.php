<?php

namespace App\Admin\Controllers;

use App\Models\EmailPolicy;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class EmailPolicyController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'EmailPolicy';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new EmailPolicy());

        $grid->column('id', __('Id'));
        $grid->column('user_id', __('User id'));
        $grid->column('new_challenge', __('New challenge'));
        $grid->column('news', __('News'));
        $grid->column('career', __('Career'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(EmailPolicy::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('user_id', __('User id'));
        $show->field('new_challenge', __('New challenge'));
        $show->field('news', __('News'));
        $show->field('career', __('Career'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new EmailPolicy());

        $form->number('user_id', __('User id'));
        $form->switch('new_challenge', __('New challenge'));
        $form->switch('news', __('News'));
        $form->switch('career', __('Career'));

        return $form;
    }
}
