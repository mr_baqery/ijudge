<?php

namespace App\Admin\Controllers;

use App\Models\Test;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class TestController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Test';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Test());

        $grid->column('id', __('Id'));
        $grid->column('file_name', __('File name'));
        $grid->column('module', __('Module'));
        $grid->column('language', __('Language'));
        $grid->column('memory_limit', __('Memory limit'));
        $grid->column('time_limit', __('Time limit'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Test::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('file_name', __('File name'));
        $show->field('module', __('Module'));
        $show->field('language', __('Language'));
        $show->field('memory_limit', __('Memory limit'));
        $show->field('time_limit', __('Time limit'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Test());

        $form->text('file_name', __('File name'));
        $form->text('module', __('Module'));
        $form->text('language', __('Language'));
        $form->text('memory_limit', __('Memory limit'));
        $form->text('time_limit', __('Time limit'));

        return $form;
    }
}
