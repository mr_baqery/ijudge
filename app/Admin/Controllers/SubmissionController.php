<?php

namespace App\Admin\Controllers;

use App\Models\Submission;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class SubmissionController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Submission';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Submission());

        $grid->column('id', __('Id'));
        $grid->column('user_id', __('User id'));
        $grid->column('test_id', __('Test id'));
        $grid->column('language', __('Language'));
        $grid->column('status', __('Status'));
        $grid->column('file_url', __('File url'));
        $grid->column('file_name', __('File name'));
        $grid->column('code_file_name', __('Code file name'));
        $grid->column('result', __('Result'));
        $grid->column('point', __('Point'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Submission::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('user_id', __('User id'));
        $show->field('test_id', __('Test id'));
        $show->field('language', __('Language'));
        $show->field('status', __('Status'));
        $show->field('file_url', __('File url'));
        $show->field('file_name', __('File name'));
        $show->field('code_file_name', __('Code file name'));
        $show->field('result', __('Result'));
        $show->field('point', __('Point'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Submission());

        $form->number('user_id', __('User id'));
        $form->number('test_id', __('Test id'));
        $form->text('language', __('Language'));
        $form->text('status', __('Status'));
        $form->text('file_url', __('File url'));
        $form->text('file_name', __('File name'));
        $form->text('code_file_name', __('Code file name'));
        $form->text('result', __('Result'));
        $form->number('point', __('Point'));

        return $form;
    }
}
