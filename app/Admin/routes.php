<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('app_users', UserController::class);
    $router->resource('challenges', ChallengeController::class);
    $router->resource('email-policies', EmailPolicyController::class);
    $router->resource('submissions', SubmissionController::class);
    $router->resource('tests', TestController::class);
    $router->resource('user-emails', UserEmailController::class);
    $router->resource('user-profiles', UserProfileController::class);

});
