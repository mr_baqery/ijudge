<?php

namespace App\Mail;

use App\Models\Submission;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SubmissionGraded extends Mailable
{
    use Queueable, SerializesModels;
    protected $submission;
    protected $user;

    /**
     * Create a new message instance.
     *
     * @param Submission $submission
     * @param User $user
     */
    public function __construct(Submission $submission, User $user)
    {
        $this->submission = $submission;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): SubmissionGraded
    {
        return $this->view('emails.submission.graded')
            ->with([
                "name" => $this->user->name,
                "date" => $this->submission->created_at,
                "id" => $this->submission->id,
                "result" => $this->submission->result ,
                "point" => $this->submission->point,
            ]);
    }
}
