<?php

namespace App\Exceptions;

use App\Utilities\ApiCode;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $e)
    {
        if ($request->is('api/*')) {
            if ($e instanceof ValidationException) {
                return $this->respondWithValidationError($e);
            }
            if ($e instanceof ModelNotFoundException) {
                $model = strtolower(class_basename($e->getModel()));

                return response()->json([
                    'error' => 'Model not found'
                ], 404);
            }
            if ($e instanceof NotFoundHttpException) {
                return response()->json([
                    'error' => 'Resource not found'
                ], 404);
            }
            if ($e instanceof AuthenticationException) {
                return $this->respondWithAuthenticationError($e);
            }
        }

        return parent::render($request, $e);
    }


    private function respondWithValidationError(Throwable $throwable): Response
    {
        return ResponseBuilder::asError(ApiCode::VALIDATION_ERROR)
            ->withData($throwable->errors())
            ->withHttpCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->build();
    }

    private function respondWithAuthenticationError(AuthenticationException $exception): Response
    {
        return ResponseBuilder::asError(ApiCode::AUTHENTICATION_ERROR)
            ->withData($exception->getMessage())
            ->withHttpCode(Response::HTTP_UNAUTHORIZED)
            ->build();
    }

}
