<?php

namespace App\Providers;

use App\Models\EmailPolicy;
use App\Models\UserEmail;
use App\Policies\EmailPoliciesPolicy;
use App\Policies\UserEmailPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        EmailPolicy::class => EmailPoliciesPolicy::class,
        UserEmail::class => UserEmailPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
