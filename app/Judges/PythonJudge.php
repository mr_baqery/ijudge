<?php


namespace App\Judges;


use App\Utilities\Commands;

class PythonJudge extends IJudge
{
    private $command = Commands::PYTHON_COMMAND;
    private $resultXML = "nosetests.xml";

    public function createInitFile()
    {
        chdir($this->cwd);
        mkdir($this->test->getModule());
        $content = "from {$this->test->getModule()}.main import *";
        $initFile = fopen("{$this->test->getModule()}/__init__.py", "w");
        copy(
            $this->submission->getCodeFilePath(),
            $this->submission->getProjectPath() . DIRECTORY_SEPARATOR . $this->test->getModule() . DIRECTORY_SEPARATOR . $this->submission->getCodeFileName());
        fwrite($initFile, $content);
        fclose($initFile);
    }

    public function preprocessing()
    {
        $this->createInitFile();
    }

    public function execute()
    {
        chdir($this->cwd);

        $output = null;
        $status = null;

        exec($this->command, $output, $status);
    }

    public function parseResult()
    {
        chdir($this->cwd);
        return simplexml_load_file($this->resultXML)->asXML();
    }

    public function installRequirements()
    {
        // TODO: Implement installRequirements() method.
    }

    public function run()
    {
        $this->bringThemAllTogether();
        $this->unZipProjects();
        $this->preprocessing();
        $this->execute();
    }
}
