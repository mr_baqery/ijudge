<?php


namespace App\Judges;


use App\Models\Submission;
use App\Models\Test;
use App\Models\User;

abstract class IJudge
{
    /**
     * @var string
     */
    protected $cwd;

    /**
     * @var string
     */
    protected $project_type;

    protected $version;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var Submission
     */
    protected $submission;

    /**
     * @var Test
     */
    protected $test;

    public function __construct(Test $test, Submission $submission, User $user, string $project_type)
    {
        $this->test = $test;
        $this->submission = $submission;
        $this->user = $user;
        $this->project_type = $project_type;
        $this->cwd = $this->submission->getProjectPath();
    }

    /**
     * Bring test project and submission file beside together
     */
    protected function bringThemAllTogether()
    {
        $dest = $this->submission->getProjectPath();
        $source = $this->test->getProjectFile();
        copy($source, $dest . DIRECTORY_SEPARATOR . $this->test->getFileName());
    }

    /**
     * Unzip project
     */
    protected function unZipProjects()
    {
        chdir($this->cwd);
        $zipFiles = glob($this->submission->getProjectPath() . DIRECTORY_SEPARATOR . "*.zip");
        $zip = new \ZipArchive();

        foreach ($zipFiles as $zipFile) {
            if ($zip->open($zipFile)) {
                $zip->extractTo($this->submission->getProjectPath());
            }
        }
    }

    public abstract function preprocessing();

    /**
     * Execute the project
     */
    public abstract function execute();

    /**
     * Parse result of test execution
     */
    public abstract function parseResult();

    /**
     * Install requirements
     */
    public abstract function installRequirements();

    /**
     * Run judge
     */
    public abstract function run();
}
