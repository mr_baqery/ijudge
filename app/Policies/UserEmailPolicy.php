<?php

namespace App\Policies;

use App\Models\User;
use App\Models\UserEmail;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserEmailPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user): bool
    {
        return false;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param UserEmail $userEmail
     * @return mixed
     */
    public function view(User $user, UserEmail $userEmail): bool
    {
        return $user->id === $userEmail->user_id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param UserEmail $userEmail
     * @return mixed
     */
    public function update(User $user, UserEmail $userEmail): bool
    {
        return $user->id === $userEmail->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param UserEmail $userEmail
     * @return mixed
     */
    public function delete(User $user, UserEmail $userEmail): bool
    {
        return $user->id === $userEmail->user_id;
    }

}
