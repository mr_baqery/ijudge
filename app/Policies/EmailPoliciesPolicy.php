<?php

namespace App\Policies;

use App\Models\EmailPolicy;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmailPoliciesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user): bool
    {
        return false;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param EmailPolicy $emailPolicy
     * @return mixed
     */
    public function view(User $user, EmailPolicy $emailPolicy): bool
    {
        return $user->id === $emailPolicy->user_id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user): bool
    {
        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param EmailPolicy $emailPolicy
     * @return mixed
     */
    public function update(User $user, EmailPolicy $emailPolicy): bool
    {
        return $user->id === $emailPolicy->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param EmailPolicy $emailPolicy
     * @return mixed
     */
    public function delete(User $user, EmailPolicy $emailPolicy): bool
    {
        return false;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param User $user
     * @param EmailPolicy $emailPolicy
     * @return mixed
     */
    public function restore(User $user, EmailPolicy $emailPolicy): bool
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param User $user
     * @param EmailPolicy $emailPolicy
     * @return mixed
     */
    public function forceDelete(User $user, EmailPolicy $emailPolicy)
    {
        //
    }
}
