<?php

namespace App\Jobs;

use App\Judges\PythonJudge;
use App\Mail\SubmissionGraded;
use App\Models\Submission;
use App\Models\Test;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class PythonSubmissionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Test
     */
    protected $test;

    /**
     * @var Submission
     */
    protected $submission;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var string
     */
    protected $project_type = "python";

    /**
     * Create a new job instance.
     * @param Test $test
     * @param Submission $submission
     * @param User $user
     */
    public function __construct(Test $test, Submission $submission, User $user)
    {
        $this->test = $test;
        $this->submission = $submission;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $pythonJudge = new PythonJudge($this->test, $this->submission, $this->user, $this->project_type);
        $pythonJudge->run();
        $this->submission->result = "Graded";
        $this->submission->status = "Finished";
        $this->submission->point = 100;
        $this->submission->save();

        $user = $this->submission->user()->first();
        Mail::to($user)->send(new SubmissionGraded($this->submission, $user));
    }
}
