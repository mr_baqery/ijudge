<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }


    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }

    /**
     * Encrypt password before storing the user in database
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes["password"] = Hash::make($password);
    }

    public function getUpdatedAtAttribute(): string
    {
        return Carbon::parse($this->attributes["updated_at"])->toDateTimeString();
    }

    public function getCreatedAtAttribute(): string
    {
        return Carbon::parse($this->attributes['created_at'])->toDateTimeString();
    }

    public function getEmailVerifiedAtAttribute(): ?string
    {
        if ($this->attributes['email_verified_at']) {
            return Carbon::parse($this->attributes['email_verified_at'])->toDateTimeString();
        }
        return null;
    }


    /**
     * User::byEmail('test@test.com')->first()
     * why first ?  in order to fetch the record.
     * @param Builder $query
     * @param string $email
     * @return Builder
     */
    public function scopeByEmail(Builder $query, string $email): Builder
    {
        return $query->where('email', '=', $email);
    }

    public function profile(): HasOne
    {
        return $this->hasOne(UserProfile::class);
    }

    public function emails(): HasMany
    {
        return $this->hasMany(UserEmail::class);
    }

    public function submissions(): HasMany
    {
        return $this->hasMany(Submission::class);
    }

    public function emailPolicies(): HasOne
    {
        return $this->hasOne(EmailPolicy::class);
    }

    public function getActiveEmail()
    {
        return $this->emails()->where("active", '=', 1)->first();
    }
}
