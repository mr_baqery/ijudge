<?php

namespace App\Models;

use App\Utilities\Constants;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Storage;
use SSD\DotEnv\DotEnv;

class Test extends Model
{
    use HasFactory;

    protected $fillable = [
        "file_name",
        "module",
        "language",
        "memory_limit",
        "time_limit"
    ];

    public function getUpdatedAtAttribute(): string
    {
        return Carbon::parse($this->attributes["updated_at"])->toDateTimeString();
    }

    public function getCreatedAtAttribute(): string
    {
        return Carbon::parse($this->attributes['created_at'])->toDateTimeString();
    }

    public function challenge(): HasOne
    {
        return $this->hasOne(Challenge::class);
    }

    public function submissions(): HasMany
    {
        return $this->hasMany(Submission::class);
    }

    public function projectId()
    {
        return $this->id;
    }

    public function getFileName(): string
    {
        return $this->file_name;
    }

    public function getDir(): string
    {
        return Constants::TESTS_DIRECTORY;
    }

    public function getProjectPath(): string
    {
        $storage = Constants::STORAGE;
        return ($storage . DIRECTORY_SEPARATOR . $this->getDir() . DIRECTORY_SEPARATOR . $this->projectId());
    }

    public function getProjectFile(): string
    {
        $storage = Constants::STORAGE;
        return ($storage . DIRECTORY_SEPARATOR . $this->getDir() . DIRECTORY_SEPARATOR . $this->projectId() . DIRECTORY_SEPARATOR . $this->getFileName());
    }

    public function getModule()
    {
        return $this->module;
    }
}
