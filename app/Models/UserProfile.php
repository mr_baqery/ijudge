<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserProfile extends Model
{
    use HasFactory;

    protected $fillable = [
        "total_point",
        "profile_pic",
        "university",
        "state",
        "phone_no",
        "private_profile",
        "username",
    ];


    public function getUpdatedAtAttribute(): string
    {
        return Carbon::parse($this->attributes["updated_at"])->toDateTimeString();
    }

    public function getCreatedAtAttribute(): string
    {
        return Carbon::parse($this->attributes['created_at'])->toDateTimeString();
    }


    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
