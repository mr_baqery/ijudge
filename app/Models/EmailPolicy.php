<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class EmailPolicy extends Model
{
    use HasFactory;

    protected $fillable = [
        "new_challenge",
        "news",
        "career",
    ];


    public function getUpdatedAtAttribute(): string
    {
        return Carbon::parse($this->attributes["updated_at"])->toDateTimeString();
    }

    public function getCreatedAtAttribute(): string
    {
        return Carbon::parse($this->attributes['created_at'])->toDateTimeString();
    }


    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

}
