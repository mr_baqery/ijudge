<?php

namespace App\Models;

use App\Utilities\Constants;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;
use SSD\DotEnv\DotEnv;

class Submission extends Model
{
    use HasFactory;

    protected $fillable = [
        "language",
        "status",
        "file_url",
        "file_name",
        "code_file_name",
        "result",
        "point",
        "test_id"
    ];

    public function test(): BelongsTo
    {
        return $this->belongsTo(Test::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function getUpdatedAtAttribute(): string
    {
        return Carbon::parse($this->attributes["updated_at"])->toDateTimeString();
    }

    public function getCreatedAtAttribute(): string
    {
        return Carbon::parse($this->attributes['created_at'])->toDateTimeString();
    }

    public function projectId()
    {
        return $this->id;
    }

    public function getFileName(): string
    {
        return $this->file_name;
    }

    public function getDir(): string
    {
        return Constants::SUBMISSIONS_DIRECTORY;
    }

    public function getProjectPath(): string
    {
        $storage = Constants::STORAGE;
        return ($storage . DIRECTORY_SEPARATOR . $this->getDir() . DIRECTORY_SEPARATOR . $this->projectId());
    }

    /**
     * @return string
     */
    public function getCodeFilePath(): string
    {
        $storage = Constants::STORAGE;
        return ($storage . DIRECTORY_SEPARATOR . $this->getDir() . DIRECTORY_SEPARATOR . $this->projectId() . DIRECTORY_SEPARATOR . $this->getCodeFileName());
    }

    public function getCodeFileName(): string
    {
        return $this->code_file_name;
    }

    public function getProjectFile(): string
    {
        $storage = Constants::STORAGE;
        return ($storage . DIRECTORY_SEPARATOR . $this->getDir() . DIRECTORY_SEPARATOR . $this->projectId() . DIRECTORY_SEPARATOR . $this->getFileName());
    }

}
