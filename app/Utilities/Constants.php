<?php


namespace App\Utilities;


class Constants
{
    const TESTS_DIRECTORY = "tests";
    const SUBMISSIONS_DIRECTORY = "submissions";
    const STORAGE = "/home/mreza/laravel/ijudge/storage/app";
}
