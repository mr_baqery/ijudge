<?php


namespace App\Utilities;


class ApiCode
{
    public const INVALID_CREDENTIALS = 251;
    public const SOMETHING_WENT_WRONG = 250;
    public const VALIDATION_ERROR = 252;
    public const AUTHENTICATION_ERROR = 253;
    public const INVALID_EMAIL_VERIFICATION_URL = 254;
    public const EMAIL_ALREADY_VERIFIED = 255;
    public const INVALID_RESET_PASSWORD_TOKEN = 256;
    public const EMAIL_IS_NOT_VERIFIED = 257;
    public const AUTHORIZATION_ERROR = 258;
    public const EMAIL_HAS_ALREADY_TAKEN = 259;
}
