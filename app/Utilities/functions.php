<?php

use App\Models\Submission;
use App\Utilities\Constants;
use Illuminate\Support\Facades\Storage;

function getCodeFileName($file): string
{
    $za = new ZipArchive();
    $za->open($file);

    $codeFileName = "";

    for ($i = 0; $i < 1; $i++) {
        $codeFileName = $za->statIndex($i)["name"];
    }

    return $codeFileName;
}

function saveSubmission(Submission $submission, $file)
{
    Storage::put(Constants::SUBMISSIONS_DIRECTORY . DIRECTORY_SEPARATOR . $submission->id, $file);
}

function getFileUrl(Submission $submission, $submission_file_name): string
{
    return Storage::url(Constants::SUBMISSIONS_DIRECTORY . DIRECTORY_SEPARATOR .
        $submission->id . DIRECTORY_SEPARATOR . $submission_file_name);
}
