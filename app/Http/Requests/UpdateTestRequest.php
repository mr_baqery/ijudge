<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "file_name" => "string",
            "module" => "string",
            "language" => "string",
            "memory_limit" => "integer",
            "time_limit" => "integer",
            "test_project" => "file|mimes:zip"
        ];
    }
}
