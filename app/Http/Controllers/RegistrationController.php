<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterPostRequest;
use App\Http\Requests\ResendEmailVerifcationLinkRequest;
use App\Models\User;
use App\Utilities\ApiCode;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RegistrationController extends Controller
{
    public function register(RegisterPostRequest $request): Response
    {
        $data = $request->validated();

        $user = User::create($data);

        $user->profile()->create([
            "username" => $user->id
        ]);

        $user->emails()->create([
            "email" => $user->email,
            "active" => 1
        ]);

        $user->emailPolicies()->create();

        if ($user instanceof User) {
            $user->sendEmailVerificationNotification();
            return $this->respond($user, "User created successfully");
        }

        return $this->respondBadRequest(ApiCode::SOMETHING_WENT_WRONG);
    }

    public function verify(Request $request, int $user_id): Response
    {
        if (!$request->hasValidSignature()) {
            return $this->respondWithUnAuthorized(ApiCode::INVALID_EMAIL_VERIFICATION_URL);
        }
        $user = User::findOrFail($user_id);

        if (!$user instanceof User) {
            return $this->respondWithMessage("User not found!");
        }

        if (!$user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();

            return $this->respondWithMessage("User successfully verified.");
        }

        if ($user->hasVerifiedEmail()) {
            return $this->respondWithMessage("User is already verified.");

        }


        return $this->respondWithError(ApiCode::SOMETHING_WENT_WRONG, Response::HTTP_UNPROCESSABLE_ENTITY);

    }

    public function resend(ResendEmailVerifcationLinkRequest $request): Response
    {
        $email = $request->get('email');

        $user = User::byEmail($email)->first();

        if ($user->hasVerifiedEmail()) {
            return $this->respondBadRequest(ApiCode::EMAIL_ALREADY_VERIFIED);
        }

        $user->sendEmailVerificationNotification();

        return $this->respondWithMessage("Verification email is now sent to your email.");
    }

}
