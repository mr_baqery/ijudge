<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmailPolicyUpdateRequest;
use App\Models\EmailPolicy;
use App\Utilities\ApiCode;
use Symfony\Component\HttpFoundation\Response;

class EmailPolicyController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth:api");
    }

    /**
     * @return Response
     */
    public function show(): Response
    {
        $user = auth()->user();

        $emailPolicies = $user->emailPolicies()->first();

        // Uncomment this line to test policy class.
        // $emailPolicies = EmailPolicy::findOrFail(3);

        if ($user->cannot("view", $emailPolicies)){
            return $this->respondWithUnAuthorized(ApiCode::AUTHORIZATION_ERROR);
        }

        return $this->respond($emailPolicies);
    }

    /**
     * @param EmailPolicyUpdateRequest $request
     * @return Response
     */
    public function update(EmailPolicyUpdateRequest $request): Response
    {
        $data = $request->validated();

        $user = auth()->user();

        $emailPolicies = $user->emailPolicies()->first();

        // Uncomment this line to test policy class.
        // $emailPolicies = EmailPolicy::findOrFail(3);

        if ($user->cannot("update", $emailPolicies)){
            return $this->respondWithUnAuthorized(ApiCode::AUTHORIZATION_ERROR);
        }

        $emailPolicies->update($data);

        return $this->respond($emailPolicies);

    }

}
