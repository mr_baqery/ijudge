<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewPasswordRequest;
use App\Http\Requests\SendResetPasswordRequest;
use App\Utilities\ApiCode;
use Illuminate\Support\Facades\Password;
use Symfony\Component\HttpFoundation\Response;

class ForgetPasswordController extends Controller
{
    public function forget(SendResetPasswordRequest $request): Response
    {
        $credential = $request->validated();

        $status = Password::sendResetLink($credential);

        if ($status === Password::INVALID_USER) {
            return $this->respondBadRequest(ApiCode::INVALID_CREDENTIALS);
        } elseif ($status === Password::RESET_THROTTLED) {
            return $this->respondWithMessage("To many request");
        }

        return $this->respondWithMessage("Reset password link has been send.");

    }

    public function reset(NewPasswordRequest $request): Response
    {
        $credentials = $request->validated();

        $email_password_status = Password::reset($credentials, function ($user, $password) {
            $user->password = $password;
            $user->save();
        });

        if ($email_password_status === Password::INVALID_TOKEN) {
            return $this->respondBadRequest(ApiCode::INVALID_RESET_PASSWORD_TOKEN);
        } elseif ($email_password_status === Password::INVALID_USER) {
            return $this->respondBadRequest(ApiCode::INVALID_CREDENTIALS);
        }

        return $this->respondWithMessage("Password successfully changed.");
    }

}
