<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserProfileUpdateRequest;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api");
    }

    /**
     * @return Response
     */
    public function show(): Response
    {
        $user = auth()->user();

        $userProfile = $user->profile()->first();

        return $this->respond($userProfile);
    }


    /**
     * @param UserProfileUpdateRequest $request
     * @return Response
     */
    public function update(UserProfileUpdateRequest $request): Response
    {
        $data = $request->validated();

        $user = auth()->user();

        $userProfile = $user->profile()->first();

        $userProfile->update($data);

        return $this->respond($userProfile);

    }


}
