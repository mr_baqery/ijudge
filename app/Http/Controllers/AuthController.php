<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Utilities\ApiCode;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except(['login']);
    }


    /**
     * @param LoginRequest $request
     * @return Response
     */
    public function login(LoginRequest $request): Response
    {
        $credentials = $request->validated();

        if (!$token = auth()->attempt($credentials)) {
            return $this->respondUnAuthenticated(ApiCode::INVALID_CREDENTIALS);
        }

        $user = auth()->user();

        if (!$user->hasVerifiedEmail()) {
            return $this->respondWithError(ApiCode::EMAIL_IS_NOT_VERIFIED, Response::HTTP_UNAUTHORIZED);
        }

        return $this->respondWithToken($token);
    }

    public function respondWithToken($token): Response
    {
        return $this->respond([
            "token" => $token,
            "access_type" => "bearer",
            "expires_in" => auth()->factory()->getTTL() * 60
        ]);
    }

    public function logout(): Response
    {
        auth()->logout();

        return $this->respondWithMessage("User successfully logged out.");
    }

    public function refresh()
    {
        $token = auth()->refresh();
        return $this->respondWithToken($token);
    }

    public function me()
    {
        return $this->respond(auth()->user());
    }

}
