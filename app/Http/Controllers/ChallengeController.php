<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChallengeCreateRequest;
use App\Http\Requests\ChallengeUpdateRequest;
use App\Models\Challenge;
use App\Models\Test;
use App\Utilities\ApiCode;
use Symfony\Component\HttpFoundation\Response;

class ChallengeController extends Controller
{

    public function store(ChallengeCreateRequest $request, Test $test): Response
    {
        if ($test->challenge()->first() instanceof Challenge){
            return $this->respondWithErrorMessage(ApiCode::SOMETHING_WENT_WRONG, "This test has already a challenge.");
        }

        $data = $request->validated();

        $test->challenge()->create($data);

        $test = Test::with('challenge')->where('id', '=', $test->id)->get();

        return $this->respond($test);
    }

    /**
     * @param ChallengeUpdateRequest $request
     * @param Test $test
     * @return Response
     */
    public function update(ChallengeUpdateRequest $request, Test $test): Response
    {
        $data = $request->validated();

        $test->challenge()->update($data);

        $test = Test::with('challenge')->where('id', '=', $test->id)->get();

        return $this->respond($test);
    }

}
