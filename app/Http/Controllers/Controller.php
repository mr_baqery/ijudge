<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;
use Symfony\Component\HttpFoundation\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function respond($data, $message = null): Response
    {
        return ResponseBuilder::asSuccess()
            ->withData($data)
            ->withMessage($message)
            ->build();
    }

    public function respondWithMessage($message): Response
    {
        return ResponseBuilder::asSuccess()
            ->withMessage($message)
            ->build();
    }

    public function respondWithError($code, $http_code): Response
    {
        return ResponseBuilder::asError($code)
            ->withHttpCode($http_code)
            ->build();
    }

    public function respondWithErrorMessage($code, $message) : Response
    {
        return ResponseBuilder::asError($code)
            ->withMessage($message)
            ->build();
    }

    public function respondBadRequest($api_code): Response
    {
        return $this->respondWithError($api_code, Response::HTTP_BAD_REQUEST);
    }

    public function respondUnAuthenticated($api_code): Response
    {
        return $this->respondWithError($api_code, Response::HTTP_UNAUTHORIZED);
    }

    public function respondNotFound($api_code): Response
    {
        return $this->respondWithError($api_code, Response::HTTP_NOT_FOUND);
    }

    public function respondWithUnAuthorized($api_code): Response
    {
        return $this->respondWithError($api_code, Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
