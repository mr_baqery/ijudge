<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangeActiveEmailRequest;
use App\Http\Requests\UserEmailCreateRequest;
use App\Http\Requests\UserEmailUpdateRequest;
use App\Models\UserEmail;
use App\Utilities\ApiCode;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpFoundation\Response;

class UserEmailController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api");
    }

    public function index(): Response
    {
        $user = auth()->user();
        $userEmails = $user->emails()->get();
        return $this->respond($userEmails);
    }

    /**
     *
     * @param UserEmailCreateRequest $request
     * @return Response
     */
    public function store(UserEmailCreateRequest $request): Response
    {
        $user = auth()->user();

        $data = $request->validated();

        $userEmail = $user->emails()->create($data);

        return $this->respond($userEmail);
    }

    /**
     *
     * @param UserEmail $userEmail
     * @return Response
     */
    public function show(UserEmail $userEmail): Response
    {
        $user = auth()->user();
        if ($user->cannot("view", $userEmail)) {
            return $this->respondWithUnAuthorized(ApiCode::AUTHORIZATION_ERROR);
        }
        return $this->respond($userEmail);
    }


    /**
     *
     * @param UserEmailUpdateRequest $request
     * @param UserEmail $userEmail
     * @return Response
     */
    public function update(UserEmailUpdateRequest $request, UserEmail $userEmail): Response
    {
        $user = auth()->user();

        if ($user->cannot("update", $userEmail)) {
            return $this->respondWithUnAuthorized(ApiCode::AUTHORIZATION_ERROR);
        }

        $data = $request->validated();
        $email = $data["email"];

        $userEmailFound = UserEmail::byEmail($email)->first();

        if ($userEmailFound instanceof UserEmail && $userEmailFound->id !== $userEmail->id) {
            return $this->respondWithError(ApiCode::EMAIL_HAS_ALREADY_TAKEN, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $userEmail->update($data);

        return $this->respond($userEmail);
    }

    public function changeActiveEmail(ChangeActiveEmailRequest $request): Response
    {
        $user = auth()->user();

        $email = $request->input("email");

        $mustBeActivatedEmail = $user->emails()->where("email", '=', $email)->first();

        if (!$mustBeActivatedEmail instanceof UserEmail) {
            return $this->respondWithErrorMessage(ApiCode::SOMETHING_WENT_WRONG, "There is no such email associated with this user");
        }

        $activeEmail = $user->getActiveEmail();
        $activeEmail->active= false;
        $activeEmail->save();

        $mustBeActivatedEmail->active = true;
        $mustBeActivatedEmail->save();

        return $this->respond($mustBeActivatedEmail);
    }

}
