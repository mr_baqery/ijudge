<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubmissionCreateRequest;
use App\Jobs\PythonSubmissionJob;
use App\Models\Submission;
use App\Models\Test;
use App\Utilities\Constants;
use Exception;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use ZipArchive;

class SubmissionController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api");
    }

    public function index(): Response
    {

        $submissions = auth()->user()->submissions()->get();

        return $this->respond($submissions);
    }

    protected function getCodeFileName($file): string
    {
        $za = new ZipArchive();
        $za->open($file);

        $codeFileName = "";

        for ($i = 0; $i < 1; $i++) {
            $codeFileName = $za->statIndex($i)["name"];
        }

        return $codeFileName;
    }

    protected function saveSubmission(Submission $submission, $file)
    {
        Storage::put(Constants::SUBMISSIONS_DIRECTORY . DIRECTORY_SEPARATOR . $submission->id, $file);
    }

    protected function getFileUrl(Submission $submission, $submission_file_name): string
    {
        return Storage::url(Constants::SUBMISSIONS_DIRECTORY . DIRECTORY_SEPARATOR .
            $submission->id . DIRECTORY_SEPARATOR . $submission_file_name);
    }

    /**
     * @param SubmissionCreateRequest $request
     * @param Test $test
     * @return Response
     */
    public function store(SubmissionCreateRequest $request, Test $test): Response
    {
        $user = auth()->user();

        $data = $request->validated();

        $data["status"] = "Pending";
        $data["result"] = "Grading";
        $data["point"] = 0;
        $data["file_url"] = "test";
        $submission_file = $data["submission_file"];
        $data["file_name"] = $submission_file->hashName();
        $data["test_id"] = $test->id;
        $data["code_file_name"] = "test";

        $submission = $user->submissions()->create($data);

        $this->saveSubmission($submission, $submission_file);
        $submission->file_url = $this->getFileUrl($submission, $data["file_name"]);
        $submission->code_file_name = $this->getCodeFileName($submission_file);
        $submission->save();

        PythonSubmissionJob::dispatch($test, $submission, $user);

        return $this->respond($submission);
    }

    /**
     * @param Submission $submission
     * @return Response
     */
    public function show(Test $test, Submission $submission): Response
    {
        return $this->respond($submission);
    }


    /**
     *
     * @param Submission $submission
     * @return Response
     * @throws Exception
     */
    public function destroy(Submission $submission): Response
    {
        Storage::deleteDirectory(Constants::SUBMISSIONS_DIRECTORY . DIRECTORY_SEPARATOR . $submission->id);

        $submission->delete();

        return $this->respondWithMessage("Resource deleted successfully");
    }
}
