<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTestRequest;
use App\Http\Requests\UpdateTestRequest;
use App\Models\Test;
use App\Utilities\Constants;
use Exception;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class TestController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * @return Response
     */
    public function index(): Response
    {
        $tests = Test::with(['challenge'])->get();
        return $this->respond($tests);
    }


    /**
     * @param CreateTestRequest $request
     * @return Response
     */
    public function store(CreateTestRequest $request): Response
    {
        $data = $request->validated();

        if (!key_exists("memory_limit", $data)) {
            $data["memory_limit"] = 128;
        }

        if (!key_exists("time_limit", $data)) {
            $data["time_limit"] = 6;
        }

        $testProject = $data["test_project"];
        $testProjectName = $testProject->hashName();
        $data["file_name"] = $testProjectName;
        $test = Test::create($data);

        Storage::put(Constants::TESTS_DIRECTORY . DIRECTORY_SEPARATOR . $test->id, $testProject);
        return $this->respond($test);
    }

    /**
     * @param Test $test
     * @return Response
     */
    public function show(Test $test): Response
    {
        $test = Test::with(['challenge'])->where('id', '=', $test->id);
        return $this->respond($test);
    }


    /**
     * @param UpdateTestRequest $request
     * @param Test $test
     * @return Response
     */
    public function update(UpdateTestRequest $request, Test $test): Response
    {
        $prevTestProjectName = $test->file_name;

        $data = $request->validated();
        if (key_exists("test_project", $data)) {
            Storage::delete(Constants::TESTS_DIRECTORY . DIRECTORY_SEPARATOR . $test->id . DIRECTORY_SEPARATOR . $prevTestProjectName);

            $testProject = $data["test_project"];
            $data["file_name"] = $testProject->hashName();
            Storage::put(Constants::TESTS_DIRECTORY . DIRECTORY_SEPARATOR . $test->id, $testProject);
        }

        $test->update($data);

        return $this->respond($test);
    }

    /**
     * @param Test $test
     * @return Response
     * @throws Exception
     */
    public function destroy(Test $test): Response
    {
        Storage::deleteDirectory(
            Constants::TESTS_DIRECTORY . DIRECTORY_SEPARATOR . $test->id
        );
        $test->delete();

        return $this->respondWithMessage("Resource successfully deleted.");
    }
}
