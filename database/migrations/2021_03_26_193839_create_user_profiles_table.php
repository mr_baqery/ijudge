<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->id();
            $table->foreignId("user_id");
            $table->integer("total_point")->default(0);
            $table->string("profile_pic")->nullable();
            $table->string("university")->nullable();
            $table->string("state")->nullable();
            $table->string("phone_no")->nullable();
            $table->tinyInteger("private_profile")->default(0);
            $table->string("username")->unique();
            $table->timestamps();

            $table->foreign("user_id")
                ->references("id")
                ->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
