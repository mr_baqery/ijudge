<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submissions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('test_id');
            $table->string("language");
            $table->string('status');
            $table->string('file_url');
            $table->string('file_name');
            $table->string("code_file_name");
            $table->string('result');
            $table->integer("point");
            $table->timestamps();

            $table->foreign("user_id")
                ->references('id')
                ->on('users');
            $table->foreign('test_id')
                ->references('id')
                ->on('tests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submissions');
    }
}
