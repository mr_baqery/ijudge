<?php


return [
    'invalid_credentials' => "Invalid email or password.",
    'something_credentials' => "Oops! something went wrong.",
    'validation_error' => "Validation Error.",
    'authentication_error' => "Unauthenticated.",
    "invalid_email_verification_url" => "The url specified is not valid.",
    'email_already_verified' => "The email is already verified.",
    'invalid_reset_password_token' => 'The provided token is not valid.',
    "email_is_not_verified" => "Your email has not been verified yet.",
    "authorization_error" => "Operation is not authorized.",
    "email_has_already_taken" => "Email has already taken"
];
