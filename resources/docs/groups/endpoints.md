# Endpoints


## Handle a login request.




> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/admin/auth/login" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/admin/auth/login"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/admin/auth/login'
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('POST', url, headers=headers)
response.json()
```


> Example response (419):

```json
{
    "message": "CSRF token mismatch.",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\HttpException",
    "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Exceptions\/Handler.php",
    "line": 385,
    "trace": [
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Exceptions\/Handler.php",
            "line": 330,
            "function": "prepareException",
            "class": "Illuminate\\Foundation\\Exceptions\\Handler",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/app\/Exceptions\/Handler.php",
            "line": 72,
            "function": "render",
            "class": "Illuminate\\Foundation\\Exceptions\\Handler",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/nunomaduro\/collision\/src\/Adapters\/Laravel\/ExceptionHandler.php",
            "line": 54,
            "function": "render",
            "class": "App\\Exceptions\\Handler",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 51,
            "function": "render",
            "class": "NunoMaduro\\Collision\\Adapters\\Laravel\\ExceptionHandler",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 172,
            "function": "handleException",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/View\/Middleware\/ShareErrorsFromSession.php",
            "line": 49,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\View\\Middleware\\ShareErrorsFromSession",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/StartSession.php",
            "line": 121,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/StartSession.php",
            "line": 63,
            "function": "handleStatefulRequest",
            "class": "Illuminate\\Session\\Middleware\\StartSession",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\StartSession",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/AddQueuedCookiesToResponse.php",
            "line": 37,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/EncryptCookies.php",
            "line": 67,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\EncryptCookies",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 695,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 670,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 636,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 166,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 128,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/PreventRequestsDuringMaintenance.php",
            "line": 86,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/fruitcake\/laravel-cors\/src\/HandleCors.php",
            "line": 37,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/fruitcake\/laravel-cors\/src\/HandleCors.php",
            "line": 37,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 141,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 110,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 324,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 305,
            "function": "callLaravelOrLumenRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 76,
            "function": "makeApiCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 51,
            "function": "makeResponseCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 41,
            "function": "makeResponseCallIfEnabledAndNoSuccessResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 236,
            "function": "__invoke",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 172,
            "function": "iterateThroughStrategies",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 127,
            "function": "fetchResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
            "line": 119,
            "function": "processRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
            "line": 73,
            "function": "processRoutes",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 36,
            "function": "handle",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Util.php",
            "line": 40,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 93,
            "function": "unwrapIfClosure",
            "class": "Illuminate\\Container\\Util",
            "type": "::"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 37,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 611,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 136,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 256,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 121,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/symfony\/console\/Application.php",
            "line": 971,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/symfony\/console\/Application.php",
            "line": 290,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/symfony\/console\/Application.php",
            "line": 166,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 92,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 129,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```
<div id="execution-results-POSTadmin-auth-login" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTadmin-auth-login"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTadmin-auth-login"></code></pre>
</div>
<div id="execution-error-POSTadmin-auth-login" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTadmin-auth-login"></code></pre>
</div>
<form id="form-POSTadmin-auth-login" data-method="POST" data-path="admin/auth/login" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTadmin-auth-login', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTadmin-auth-login" onclick="tryItOut('POSTadmin-auth-login');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTadmin-auth-login" onclick="cancelTryOut('POSTadmin-auth-login');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTadmin-auth-login" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>admin/auth/login</code></b>
</p>
</form>


## Update user setting.




> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/admin/auth/setting" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/admin/auth/setting"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};


fetch(url, {
    method: "PUT",
    headers,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/admin/auth/setting'
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('PUT', url, headers=headers)
response.json()
```


> Example response (419):

```json
{
    "message": "CSRF token mismatch.",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\HttpException",
    "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Exceptions\/Handler.php",
    "line": 385,
    "trace": [
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Exceptions\/Handler.php",
            "line": 330,
            "function": "prepareException",
            "class": "Illuminate\\Foundation\\Exceptions\\Handler",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/app\/Exceptions\/Handler.php",
            "line": 72,
            "function": "render",
            "class": "Illuminate\\Foundation\\Exceptions\\Handler",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/nunomaduro\/collision\/src\/Adapters\/Laravel\/ExceptionHandler.php",
            "line": 54,
            "function": "render",
            "class": "App\\Exceptions\\Handler",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Pipeline.php",
            "line": 51,
            "function": "render",
            "class": "NunoMaduro\\Collision\\Adapters\\Laravel\\ExceptionHandler",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 172,
            "function": "handleException",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/View\/Middleware\/ShareErrorsFromSession.php",
            "line": 49,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\View\\Middleware\\ShareErrorsFromSession",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/StartSession.php",
            "line": 121,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Session\/Middleware\/StartSession.php",
            "line": 63,
            "function": "handleStatefulRequest",
            "class": "Illuminate\\Session\\Middleware\\StartSession",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Session\\Middleware\\StartSession",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/AddQueuedCookiesToResponse.php",
            "line": 37,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Cookie\/Middleware\/EncryptCookies.php",
            "line": 67,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Cookie\\Middleware\\EncryptCookies",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 695,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 670,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 636,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 166,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 128,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/PreventRequestsDuringMaintenance.php",
            "line": 86,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/fruitcake\/laravel-cors\/src\/HandleCors.php",
            "line": 37,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/fruitcake\/laravel-cors\/src\/HandleCors.php",
            "line": 37,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 141,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 110,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 324,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 305,
            "function": "callLaravelOrLumenRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 76,
            "function": "makeApiCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 51,
            "function": "makeResponseCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 41,
            "function": "makeResponseCallIfEnabledAndNoSuccessResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 236,
            "function": "__invoke",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 172,
            "function": "iterateThroughStrategies",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 127,
            "function": "fetchResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
            "line": 119,
            "function": "processRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
            "line": 73,
            "function": "processRoutes",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 36,
            "function": "handle",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Util.php",
            "line": 40,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 93,
            "function": "unwrapIfClosure",
            "class": "Illuminate\\Container\\Util",
            "type": "::"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 37,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 611,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 136,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 256,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 121,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/symfony\/console\/Application.php",
            "line": 971,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/symfony\/console\/Application.php",
            "line": 290,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/symfony\/console\/Application.php",
            "line": 166,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 92,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 129,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```
<div id="execution-results-PUTadmin-auth-setting" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTadmin-auth-setting"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTadmin-auth-setting"></code></pre>
</div>
<div id="execution-error-PUTadmin-auth-setting" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTadmin-auth-setting"></code></pre>
</div>
<form id="form-PUTadmin-auth-setting" data-method="PUT" data-path="admin/auth/setting" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTadmin-auth-setting', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTadmin-auth-setting" onclick="tryItOut('PUTadmin-auth-setting');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTadmin-auth-setting" onclick="cancelTryOut('PUTadmin-auth-setting');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTadmin-auth-setting" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>admin/auth/setting</code></b>
</p>
</form>


## api/auth/login




> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/api/auth/login" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"email":"xavier76@example.net","password":"vero"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/auth/login"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};

let body = {
    "email": "xavier76@example.net",
    "password": "vero"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/auth/login'
payload = {
    "email": "xavier76@example.net",
    "password": "vero"
}
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('POST', url, headers=headers, json=payload)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 251,
    "locale": "en",
    "message": "Invalid email or password.",
    "data": null,
    "debug": []
}
```
<div id="execution-results-POSTapi-auth-login" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-auth-login"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-auth-login"></code></pre>
</div>
<div id="execution-error-POSTapi-auth-login" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-auth-login"></code></pre>
</div>
<form id="form-POSTapi-auth-login" data-method="POST" data-path="api/auth/login" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-auth-login', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-auth-login" onclick="tryItOut('POSTapi-auth-login');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-auth-login" onclick="cancelTryOut('POSTapi-auth-login');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-auth-login" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/auth/login</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-auth-login" data-component="body" required  hidden>
<br>
The value must be a valid email address.</p>
<p>
<b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="password" data-endpoint="POSTapi-auth-login" data-component="body" required  hidden>
<br>
</p>

</form>


## api/auth/logout




> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/auth/logout" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/auth/logout"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/auth/logout'
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-GETapi-auth-logout" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-auth-logout"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-auth-logout"></code></pre>
</div>
<div id="execution-error-GETapi-auth-logout" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-auth-logout"></code></pre>
</div>
<form id="form-GETapi-auth-logout" data-method="GET" data-path="api/auth/logout" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-auth-logout', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-auth-logout" onclick="tryItOut('GETapi-auth-logout');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-auth-logout" onclick="cancelTryOut('GETapi-auth-logout');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-auth-logout" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/auth/logout</code></b>
</p>
</form>


## api/auth/refresh




> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/auth/refresh" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/auth/refresh"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/auth/refresh'
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-GETapi-auth-refresh" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-auth-refresh"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-auth-refresh"></code></pre>
</div>
<div id="execution-error-GETapi-auth-refresh" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-auth-refresh"></code></pre>
</div>
<form id="form-GETapi-auth-refresh" data-method="GET" data-path="api/auth/refresh" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-auth-refresh', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-auth-refresh" onclick="tryItOut('GETapi-auth-refresh');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-auth-refresh" onclick="cancelTryOut('GETapi-auth-refresh');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-auth-refresh" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/auth/refresh</code></b>
</p>
</form>


## api/auth/me




> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/auth/me" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/auth/me"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/auth/me'
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-GETapi-auth-me" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-auth-me"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-auth-me"></code></pre>
</div>
<div id="execution-error-GETapi-auth-me" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-auth-me"></code></pre>
</div>
<form id="form-GETapi-auth-me" data-method="GET" data-path="api/auth/me" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-auth-me', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-auth-me" onclick="tryItOut('GETapi-auth-me');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-auth-me" onclick="cancelTryOut('GETapi-auth-me');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-auth-me" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/auth/me</code></b>
</p>
</form>


## api/auth/register




> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/api/auth/register" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"name":"et","email":"vernice.wyman@example.net","password":"est","password_confirmation":"eos"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/auth/register"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};

let body = {
    "name": "et",
    "email": "vernice.wyman@example.net",
    "password": "est",
    "password_confirmation": "eos"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/auth/register'
payload = {
    "name": "et",
    "email": "vernice.wyman@example.net",
    "password": "est",
    "password_confirmation": "eos"
}
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('POST', url, headers=headers, json=payload)
response.json()
```


> Example response (422):

```json
{
    "success": false,
    "code": 252,
    "locale": "en",
    "message": "Validation Error.",
    "data": {
        "name": [
            "The name must be at least 8 characters."
        ],
        "password": [
            "The password must be at least 8 characters.",
            "The password confirmation does not match."
        ]
    },
    "debug": []
}
```
<div id="execution-results-POSTapi-auth-register" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-auth-register"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-auth-register"></code></pre>
</div>
<div id="execution-error-POSTapi-auth-register" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-auth-register"></code></pre>
</div>
<form id="form-POSTapi-auth-register" data-method="POST" data-path="api/auth/register" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-auth-register', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-auth-register" onclick="tryItOut('POSTapi-auth-register');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-auth-register" onclick="cancelTryOut('POSTapi-auth-register');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-auth-register" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/auth/register</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="name" data-endpoint="POSTapi-auth-register" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-auth-register" data-component="body" required  hidden>
<br>
The value must be a valid email address.</p>
<p>
<b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="password" data-endpoint="POSTapi-auth-register" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>password_confirmation</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="password_confirmation" data-endpoint="POSTapi-auth-register" data-component="body" required  hidden>
<br>
</p>

</form>


## api/auth/verify/{id}




> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/auth/verify/in" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/auth/verify/in"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/auth/verify/in'
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (500):

```json
{
    "message": "App\\Http\\Controllers\\RegistrationController::verify(): Argument #2 ($user_id) must be of type int, string given, called in \/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Controller.php on line 54",
    "exception": "TypeError",
    "file": "\/home\/mreza\/laravel\/ijudge\/app\/Http\/Controllers\/RegistrationController.php",
    "line": 39,
    "trace": [
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Controller.php",
            "line": 54,
            "function": "verify",
            "class": "App\\Http\\Controllers\\RegistrationController",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/ControllerDispatcher.php",
            "line": 45,
            "function": "callAction",
            "class": "Illuminate\\Routing\\Controller",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Route.php",
            "line": 254,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\ControllerDispatcher",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Route.php",
            "line": 197,
            "function": "runController",
            "class": "Illuminate\\Routing\\Route",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 693,
            "function": "run",
            "class": "Illuminate\\Routing\\Route",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 128,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 50,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/fruitcake\/laravel-cors\/src\/HandleCors.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/ThrottleRequests.php",
            "line": 127,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/ThrottleRequests.php",
            "line": 103,
            "function": "handleRequest",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/ThrottleRequests.php",
            "line": 55,
            "function": "handleRequestUsingNamedLimiter",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 695,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 670,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 636,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 166,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 128,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/PreventRequestsDuringMaintenance.php",
            "line": 86,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/fruitcake\/laravel-cors\/src\/HandleCors.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/fruitcake\/laravel-cors\/src\/HandleCors.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 141,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 110,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 324,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 305,
            "function": "callLaravelOrLumenRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 76,
            "function": "makeApiCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 51,
            "function": "makeResponseCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 41,
            "function": "makeResponseCallIfEnabledAndNoSuccessResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 236,
            "function": "__invoke",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 172,
            "function": "iterateThroughStrategies",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 127,
            "function": "fetchResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
            "line": 119,
            "function": "processRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
            "line": 73,
            "function": "processRoutes",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 36,
            "function": "handle",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Util.php",
            "line": 40,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 93,
            "function": "unwrapIfClosure",
            "class": "Illuminate\\Container\\Util",
            "type": "::"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 37,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 611,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 136,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 256,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 121,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/symfony\/console\/Application.php",
            "line": 971,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/symfony\/console\/Application.php",
            "line": 290,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/symfony\/console\/Application.php",
            "line": 166,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 92,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 129,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```
<div id="execution-results-GETapi-auth-verify--id-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-auth-verify--id-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-auth-verify--id-"></code></pre>
</div>
<div id="execution-error-GETapi-auth-verify--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-auth-verify--id-"></code></pre>
</div>
<form id="form-GETapi-auth-verify--id-" data-method="GET" data-path="api/auth/verify/{id}" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-auth-verify--id-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-auth-verify--id-" onclick="tryItOut('GETapi-auth-verify--id-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-auth-verify--id-" onclick="cancelTryOut('GETapi-auth-verify--id-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-auth-verify--id-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/auth/verify/{id}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="GETapi-auth-verify--id-" data-component="url" required  hidden>
<br>
</p>
</form>


## api/auth/resend




> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/auth/resend" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"email":"mernser@example.org"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/auth/resend"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};

let body = {
    "email": "mernser@example.org"
}

fetch(url, {
    method: "GET",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/auth/resend'
payload = {
    "email": "mernser@example.org"
}
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('GET', url, headers=headers, json=payload)
response.json()
```


> Example response (500):

```json
{
    "message": "Call to a member function hasVerifiedEmail() on null",
    "exception": "Error",
    "file": "\/home\/mreza\/laravel\/ijudge\/app\/Http\/Controllers\/RegistrationController.php",
    "line": 72,
    "trace": [
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Controller.php",
            "line": 54,
            "function": "resend",
            "class": "App\\Http\\Controllers\\RegistrationController",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/ControllerDispatcher.php",
            "line": 45,
            "function": "callAction",
            "class": "Illuminate\\Routing\\Controller",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Route.php",
            "line": 254,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\ControllerDispatcher",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Route.php",
            "line": 197,
            "function": "runController",
            "class": "Illuminate\\Routing\\Route",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 693,
            "function": "run",
            "class": "Illuminate\\Routing\\Route",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 128,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 50,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/fruitcake\/laravel-cors\/src\/HandleCors.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/ThrottleRequests.php",
            "line": 127,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/ThrottleRequests.php",
            "line": 103,
            "function": "handleRequest",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/ThrottleRequests.php",
            "line": 55,
            "function": "handleRequestUsingNamedLimiter",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 695,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 670,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 636,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 166,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 128,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/PreventRequestsDuringMaintenance.php",
            "line": 86,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/fruitcake\/laravel-cors\/src\/HandleCors.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/fruitcake\/laravel-cors\/src\/HandleCors.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 141,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 110,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 324,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 305,
            "function": "callLaravelOrLumenRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 76,
            "function": "makeApiCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 51,
            "function": "makeResponseCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 41,
            "function": "makeResponseCallIfEnabledAndNoSuccessResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 236,
            "function": "__invoke",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 172,
            "function": "iterateThroughStrategies",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 127,
            "function": "fetchResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
            "line": 119,
            "function": "processRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
            "line": 73,
            "function": "processRoutes",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 36,
            "function": "handle",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Util.php",
            "line": 40,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 93,
            "function": "unwrapIfClosure",
            "class": "Illuminate\\Container\\Util",
            "type": "::"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 37,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 611,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 136,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 256,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 121,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/symfony\/console\/Application.php",
            "line": 971,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/symfony\/console\/Application.php",
            "line": 290,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/symfony\/console\/Application.php",
            "line": 166,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 92,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 129,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/home\/mreza\/laravel\/ijudge\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```
<div id="execution-results-GETapi-auth-resend" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-auth-resend"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-auth-resend"></code></pre>
</div>
<div id="execution-error-GETapi-auth-resend" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-auth-resend"></code></pre>
</div>
<form id="form-GETapi-auth-resend" data-method="GET" data-path="api/auth/resend" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-auth-resend', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-auth-resend" onclick="tryItOut('GETapi-auth-resend');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-auth-resend" onclick="cancelTryOut('GETapi-auth-resend');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-auth-resend" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/auth/resend</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="GETapi-auth-resend" data-component="body" required  hidden>
<br>
The value must be a valid email address.</p>

</form>


## api/auth/forget_password




> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/api/auth/forget_password" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"email":"agnes41@example.org"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/auth/forget_password"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};

let body = {
    "email": "agnes41@example.org"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/auth/forget_password'
payload = {
    "email": "agnes41@example.org"
}
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('POST', url, headers=headers, json=payload)
response.json()
```


> Example response (400):

```json
{
    "success": false,
    "code": 251,
    "locale": "en",
    "message": "Invalid email or password.",
    "data": null,
    "debug": []
}
```
<div id="execution-results-POSTapi-auth-forget_password" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-auth-forget_password"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-auth-forget_password"></code></pre>
</div>
<div id="execution-error-POSTapi-auth-forget_password" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-auth-forget_password"></code></pre>
</div>
<form id="form-POSTapi-auth-forget_password" data-method="POST" data-path="api/auth/forget_password" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-auth-forget_password', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-auth-forget_password" onclick="tryItOut('POSTapi-auth-forget_password');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-auth-forget_password" onclick="cancelTryOut('POSTapi-auth-forget_password');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-auth-forget_password" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/auth/forget_password</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-auth-forget_password" data-component="body" required  hidden>
<br>
The value must be a valid email address.</p>

</form>


## api/auth/reset_password




> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/api/auth/reset_password" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"email":"lesley70@example.net","password":"cum","token":"incidunt"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/auth/reset_password"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};

let body = {
    "email": "lesley70@example.net",
    "password": "cum",
    "token": "incidunt"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/auth/reset_password'
payload = {
    "email": "lesley70@example.net",
    "password": "cum",
    "token": "incidunt"
}
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('POST', url, headers=headers, json=payload)
response.json()
```


> Example response (422):

```json
{
    "success": false,
    "code": 252,
    "locale": "en",
    "message": "Validation Error.",
    "data": {
        "password": [
            "The password must be at least 8 characters.",
            "The password confirmation does not match."
        ]
    },
    "debug": []
}
```
<div id="execution-results-POSTapi-auth-reset_password" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-auth-reset_password"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-auth-reset_password"></code></pre>
</div>
<div id="execution-error-POSTapi-auth-reset_password" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-auth-reset_password"></code></pre>
</div>
<form id="form-POSTapi-auth-reset_password" data-method="POST" data-path="api/auth/reset_password" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-auth-reset_password', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-auth-reset_password" onclick="tryItOut('POSTapi-auth-reset_password');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-auth-reset_password" onclick="cancelTryOut('POSTapi-auth-reset_password');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-auth-reset_password" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/auth/reset_password</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-auth-reset_password" data-component="body" required  hidden>
<br>
The value must be a valid email address.</p>
<p>
<b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="password" data-endpoint="POSTapi-auth-reset_password" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="token" data-endpoint="POSTapi-auth-reset_password" data-component="body" required  hidden>
<br>
</p>

</form>


## api/email_policies




> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/api/email_policies" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"new_challenge":false,"news":false,"career":false}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/email_policies"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};

let body = {
    "new_challenge": false,
    "news": false,
    "career": false
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/email_policies'
payload = {
    "new_challenge": false,
    "news": false,
    "career": false
}
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('POST', url, headers=headers, json=payload)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-POSTapi-email_policies" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-email_policies"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-email_policies"></code></pre>
</div>
<div id="execution-error-POSTapi-email_policies" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-email_policies"></code></pre>
</div>
<form id="form-POSTapi-email_policies" data-method="POST" data-path="api/email_policies" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-email_policies', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-email_policies" onclick="tryItOut('POSTapi-email_policies');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-email_policies" onclick="cancelTryOut('POSTapi-email_policies');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-email_policies" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/email_policies</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>new_challenge</code></b>&nbsp;&nbsp;<small>boolean</small>     <i>optional</i> &nbsp;
<label data-endpoint="POSTapi-email_policies" hidden><input type="radio" name="new_challenge" value="true" data-endpoint="POSTapi-email_policies" data-component="body" ><code>true</code></label>
<label data-endpoint="POSTapi-email_policies" hidden><input type="radio" name="new_challenge" value="false" data-endpoint="POSTapi-email_policies" data-component="body" ><code>false</code></label>
<br>
</p>
<p>
<b><code>news</code></b>&nbsp;&nbsp;<small>boolean</small>     <i>optional</i> &nbsp;
<label data-endpoint="POSTapi-email_policies" hidden><input type="radio" name="news" value="true" data-endpoint="POSTapi-email_policies" data-component="body" ><code>true</code></label>
<label data-endpoint="POSTapi-email_policies" hidden><input type="radio" name="news" value="false" data-endpoint="POSTapi-email_policies" data-component="body" ><code>false</code></label>
<br>
</p>
<p>
<b><code>career</code></b>&nbsp;&nbsp;<small>boolean</small>     <i>optional</i> &nbsp;
<label data-endpoint="POSTapi-email_policies" hidden><input type="radio" name="career" value="true" data-endpoint="POSTapi-email_policies" data-component="body" ><code>true</code></label>
<label data-endpoint="POSTapi-email_policies" hidden><input type="radio" name="career" value="false" data-endpoint="POSTapi-email_policies" data-component="body" ><code>false</code></label>
<br>
</p>

</form>


## api/email_policies




> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/email_policies" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/email_policies"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/email_policies'
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-GETapi-email_policies" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-email_policies"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-email_policies"></code></pre>
</div>
<div id="execution-error-GETapi-email_policies" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-email_policies"></code></pre>
</div>
<form id="form-GETapi-email_policies" data-method="GET" data-path="api/email_policies" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-email_policies', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-email_policies" onclick="tryItOut('GETapi-email_policies');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-email_policies" onclick="cancelTryOut('GETapi-email_policies');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-email_policies" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/email_policies</code></b>
</p>
</form>


## api/user_profile




> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/user_profile" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/user_profile"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/user_profile'
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-GETapi-user_profile" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-user_profile"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-user_profile"></code></pre>
</div>
<div id="execution-error-GETapi-user_profile" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-user_profile"></code></pre>
</div>
<form id="form-GETapi-user_profile" data-method="GET" data-path="api/user_profile" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-user_profile', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-user_profile" onclick="tryItOut('GETapi-user_profile');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-user_profile" onclick="cancelTryOut('GETapi-user_profile');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-user_profile" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/user_profile</code></b>
</p>
</form>


## api/user_profile




> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/api/user_profile" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"university":"ut","state":"est","phone_no":"incidunt","private_profile":false,"username":"voluptates"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/user_profile"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};

let body = {
    "university": "ut",
    "state": "est",
    "phone_no": "incidunt",
    "private_profile": false,
    "username": "voluptates"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/user_profile'
payload = {
    "university": "ut",
    "state": "est",
    "phone_no": "incidunt",
    "private_profile": false,
    "username": "voluptates"
}
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('POST', url, headers=headers, json=payload)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-POSTapi-user_profile" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-user_profile"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-user_profile"></code></pre>
</div>
<div id="execution-error-POSTapi-user_profile" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-user_profile"></code></pre>
</div>
<form id="form-POSTapi-user_profile" data-method="POST" data-path="api/user_profile" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-user_profile', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-user_profile" onclick="tryItOut('POSTapi-user_profile');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-user_profile" onclick="cancelTryOut('POSTapi-user_profile');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-user_profile" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/user_profile</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>university</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="university" data-endpoint="POSTapi-user_profile" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>state</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="state" data-endpoint="POSTapi-user_profile" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>phone_no</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="phone_no" data-endpoint="POSTapi-user_profile" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>private_profile</code></b>&nbsp;&nbsp;<small>boolean</small>     <i>optional</i> &nbsp;
<label data-endpoint="POSTapi-user_profile" hidden><input type="radio" name="private_profile" value="true" data-endpoint="POSTapi-user_profile" data-component="body" ><code>true</code></label>
<label data-endpoint="POSTapi-user_profile" hidden><input type="radio" name="private_profile" value="false" data-endpoint="POSTapi-user_profile" data-component="body" ><code>false</code></label>
<br>
</p>
<p>
<b><code>username</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="username" data-endpoint="POSTapi-user_profile" data-component="body"  hidden>
<br>
</p>

</form>


## api/user_email




> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/user_email" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/user_email"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/user_email'
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-GETapi-user_email" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-user_email"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-user_email"></code></pre>
</div>
<div id="execution-error-GETapi-user_email" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-user_email"></code></pre>
</div>
<form id="form-GETapi-user_email" data-method="GET" data-path="api/user_email" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-user_email', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-user_email" onclick="tryItOut('GETapi-user_email');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-user_email" onclick="cancelTryOut('GETapi-user_email');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-user_email" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/user_email</code></b>
</p>
</form>


## api/user_email




> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/api/user_email" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"email":"cheyanne62@example.com"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/user_email"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};

let body = {
    "email": "cheyanne62@example.com"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/user_email'
payload = {
    "email": "cheyanne62@example.com"
}
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('POST', url, headers=headers, json=payload)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-POSTapi-user_email" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-user_email"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-user_email"></code></pre>
</div>
<div id="execution-error-POSTapi-user_email" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-user_email"></code></pre>
</div>
<form id="form-POSTapi-user_email" data-method="POST" data-path="api/user_email" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-user_email', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-user_email" onclick="tryItOut('POSTapi-user_email');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-user_email" onclick="cancelTryOut('POSTapi-user_email');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-user_email" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/user_email</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-user_email" data-component="body" required  hidden>
<br>
The value must be a valid email address.</p>

</form>


## api/user_email/{userEmail}




> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/user_email/quia" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/user_email/quia"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/user_email/quia'
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-GETapi-user_email--userEmail-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-user_email--userEmail-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-user_email--userEmail-"></code></pre>
</div>
<div id="execution-error-GETapi-user_email--userEmail-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-user_email--userEmail-"></code></pre>
</div>
<form id="form-GETapi-user_email--userEmail-" data-method="GET" data-path="api/user_email/{userEmail}" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-user_email--userEmail-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-user_email--userEmail-" onclick="tryItOut('GETapi-user_email--userEmail-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-user_email--userEmail-" onclick="cancelTryOut('GETapi-user_email--userEmail-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-user_email--userEmail-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/user_email/{userEmail}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>userEmail</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="userEmail" data-endpoint="GETapi-user_email--userEmail-" data-component="url" required  hidden>
<br>
</p>
</form>


## api/user_email/{userEmail}




> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/api/user_email/eos" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"email":"bartoletti.triston@example.org"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/user_email/eos"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};

let body = {
    "email": "bartoletti.triston@example.org"
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/user_email/eos'
payload = {
    "email": "bartoletti.triston@example.org"
}
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('PUT', url, headers=headers, json=payload)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-PUTapi-user_email--userEmail-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-user_email--userEmail-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-user_email--userEmail-"></code></pre>
</div>
<div id="execution-error-PUTapi-user_email--userEmail-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-user_email--userEmail-"></code></pre>
</div>
<form id="form-PUTapi-user_email--userEmail-" data-method="PUT" data-path="api/user_email/{userEmail}" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-user_email--userEmail-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-user_email--userEmail-" onclick="tryItOut('PUTapi-user_email--userEmail-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-user_email--userEmail-" onclick="cancelTryOut('PUTapi-user_email--userEmail-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-user_email--userEmail-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/user_email/{userEmail}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>userEmail</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="userEmail" data-endpoint="PUTapi-user_email--userEmail-" data-component="url" required  hidden>
<br>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="email" data-endpoint="PUTapi-user_email--userEmail-" data-component="body"  hidden>
<br>
The value must be a valid email address.</p>

</form>


## api/active_email




> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/api/active_email" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"email":"ken.kutch@example.org"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/active_email"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};

let body = {
    "email": "ken.kutch@example.org"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/active_email'
payload = {
    "email": "ken.kutch@example.org"
}
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('POST', url, headers=headers, json=payload)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-POSTapi-active_email" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-active_email"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-active_email"></code></pre>
</div>
<div id="execution-error-POSTapi-active_email" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-active_email"></code></pre>
</div>
<form id="form-POSTapi-active_email" data-method="POST" data-path="api/active_email" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-active_email', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-active_email" onclick="tryItOut('POSTapi-active_email');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-active_email" onclick="cancelTryOut('POSTapi-active_email');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-active_email" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/active_email</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-active_email" data-component="body" required  hidden>
<br>
The value must be a valid email address.</p>

</form>


## api/tests




> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/tests" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/tests"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/tests'
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-GETapi-tests" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-tests"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-tests"></code></pre>
</div>
<div id="execution-error-GETapi-tests" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-tests"></code></pre>
</div>
<form id="form-GETapi-tests" data-method="GET" data-path="api/tests" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-tests', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-tests" onclick="tryItOut('GETapi-tests');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-tests" onclick="cancelTryOut('GETapi-tests');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-tests" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/tests</code></b>
</p>
</form>


## api/tests




> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/api/tests" \
    -H "Accept: application/json" \
    -H "Content-Type: multipart/form-data" \
    -F "file_name=numquam" \
    -F "module=ducimus" \
    -F "language=cum" \
    -F "memory_limit=11" \
    -F "time_limit=10" \
    -F "test_project=@/tmp/phpWJIA9S" 
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/tests"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "multipart/form-data",
};

const body = new FormData();
body.append('file_name', 'numquam');
body.append('module', 'ducimus');
body.append('language', 'cum');
body.append('memory_limit', '11');
body.append('time_limit', '10');
body.append('test_project', document.querySelector('input[name="test_project"]').files[0]);

fetch(url, {
    method: "POST",
    headers,
    body,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/tests'
files = {
  'test_project': open('/tmp/phpWJIA9S', 'rb')
}
payload = {
    "file_name": "numquam",
    "module": "ducimus",
    "language": "cum",
    "memory_limit": 11,
    "time_limit": 10
}
headers = {
  'Accept': 'application/json',
  'Content-Type': 'multipart/form-data'
}

response = requests.request('POST', url, headers=headers, files=files, data=payload)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-POSTapi-tests" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-tests"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-tests"></code></pre>
</div>
<div id="execution-error-POSTapi-tests" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-tests"></code></pre>
</div>
<form id="form-POSTapi-tests" data-method="POST" data-path="api/tests" data-authed="0" data-hasfiles="1" data-headers='{"Accept":"application\/json","Content-Type":"multipart\/form-data"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-tests', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-tests" onclick="tryItOut('POSTapi-tests');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-tests" onclick="cancelTryOut('POSTapi-tests');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-tests" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/tests</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>file_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="file_name" data-endpoint="POSTapi-tests" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>module</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="module" data-endpoint="POSTapi-tests" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>language</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="language" data-endpoint="POSTapi-tests" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>memory_limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="memory_limit" data-endpoint="POSTapi-tests" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>time_limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="time_limit" data-endpoint="POSTapi-tests" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>test_project</code></b>&nbsp;&nbsp;<small>file</small>  &nbsp;
<input type="file" name="test_project" data-endpoint="POSTapi-tests" data-component="body" required  hidden>
<br>
The value must be a file.</p>

</form>


## api/test/{test}




> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/api/test/ex" \
    -H "Accept: application/json" \
    -H "Content-Type: multipart/form-data" \
    -F "file_name=ut" \
    -F "module=maxime" \
    -F "language=similique" \
    -F "memory_limit=1" \
    -F "time_limit=5" \
    -F "test_project=@/tmp/php0P6WU8" 
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/test/ex"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "multipart/form-data",
};

const body = new FormData();
body.append('file_name', 'ut');
body.append('module', 'maxime');
body.append('language', 'similique');
body.append('memory_limit', '1');
body.append('time_limit', '5');
body.append('test_project', document.querySelector('input[name="test_project"]').files[0]);

fetch(url, {
    method: "PUT",
    headers,
    body,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/test/ex'
files = {
  'test_project': open('/tmp/php0P6WU8', 'rb')
}
payload = {
    "file_name": "ut",
    "module": "maxime",
    "language": "similique",
    "memory_limit": 1,
    "time_limit": 5
}
headers = {
  'Accept': 'application/json',
  'Content-Type': 'multipart/form-data'
}

response = requests.request('PUT', url, headers=headers, files=files, data=payload)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-PUTapi-test--test-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-test--test-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-test--test-"></code></pre>
</div>
<div id="execution-error-PUTapi-test--test-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-test--test-"></code></pre>
</div>
<form id="form-PUTapi-test--test-" data-method="PUT" data-path="api/test/{test}" data-authed="0" data-hasfiles="1" data-headers='{"Accept":"application\/json","Content-Type":"multipart\/form-data"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-test--test-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-test--test-" onclick="tryItOut('PUTapi-test--test-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-test--test-" onclick="cancelTryOut('PUTapi-test--test-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-test--test-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/test/{test}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>test</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="test" data-endpoint="PUTapi-test--test-" data-component="url" required  hidden>
<br>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>file_name</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="file_name" data-endpoint="PUTapi-test--test-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>module</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="module" data-endpoint="PUTapi-test--test-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>language</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="language" data-endpoint="PUTapi-test--test-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>memory_limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="memory_limit" data-endpoint="PUTapi-test--test-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>time_limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="time_limit" data-endpoint="PUTapi-test--test-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>test_project</code></b>&nbsp;&nbsp;<small>file</small>     <i>optional</i> &nbsp;
<input type="file" name="test_project" data-endpoint="PUTapi-test--test-" data-component="body"  hidden>
<br>
The value must be a file.</p>

</form>


## api/test/{test}




> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/api/test/aut" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/test/aut"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/test/aut'
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('DELETE', url, headers=headers)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-DELETEapi-test--test-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-test--test-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-test--test-"></code></pre>
</div>
<div id="execution-error-DELETEapi-test--test-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-test--test-"></code></pre>
</div>
<form id="form-DELETEapi-test--test-" data-method="DELETE" data-path="api/test/{test}" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-test--test-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-test--test-" onclick="tryItOut('DELETEapi-test--test-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-test--test-" onclick="cancelTryOut('DELETEapi-test--test-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-test--test-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/test/{test}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>test</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="test" data-endpoint="DELETEapi-test--test-" data-component="url" required  hidden>
<br>
</p>
</form>


## api/test/{test}/challenges




> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/api/test/porro/challenges" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"title":"et","point":8,"description":"necessitatibus"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/test/porro/challenges"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};

let body = {
    "title": "et",
    "point": 8,
    "description": "necessitatibus"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/test/porro/challenges'
payload = {
    "title": "et",
    "point": 8,
    "description": "necessitatibus"
}
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('POST', url, headers=headers, json=payload)
response.json()
```


> Example response (404):

```json
{
    "error": "Model not found"
}
```
<div id="execution-results-POSTapi-test--test--challenges" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-test--test--challenges"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-test--test--challenges"></code></pre>
</div>
<div id="execution-error-POSTapi-test--test--challenges" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-test--test--challenges"></code></pre>
</div>
<form id="form-POSTapi-test--test--challenges" data-method="POST" data-path="api/test/{test}/challenges" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-test--test--challenges', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-test--test--challenges" onclick="tryItOut('POSTapi-test--test--challenges');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-test--test--challenges" onclick="cancelTryOut('POSTapi-test--test--challenges');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-test--test--challenges" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/test/{test}/challenges</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>test</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="test" data-endpoint="POSTapi-test--test--challenges" data-component="url" required  hidden>
<br>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>title</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="title" data-endpoint="POSTapi-test--test--challenges" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>point</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="point" data-endpoint="POSTapi-test--test--challenges" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>description</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="description" data-endpoint="POSTapi-test--test--challenges" data-component="body" required  hidden>
<br>
</p>

</form>


## api/test/{test}/challenge




> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/api/test/doloribus/challenge" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"title":"aspernatur","point":18,"description":"qui"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/test/doloribus/challenge"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};

let body = {
    "title": "aspernatur",
    "point": 18,
    "description": "qui"
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/test/doloribus/challenge'
payload = {
    "title": "aspernatur",
    "point": 18,
    "description": "qui"
}
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('PUT', url, headers=headers, json=payload)
response.json()
```


> Example response (404):

```json
{
    "error": "Model not found"
}
```
<div id="execution-results-PUTapi-test--test--challenge" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-test--test--challenge"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-test--test--challenge"></code></pre>
</div>
<div id="execution-error-PUTapi-test--test--challenge" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-test--test--challenge"></code></pre>
</div>
<form id="form-PUTapi-test--test--challenge" data-method="PUT" data-path="api/test/{test}/challenge" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-test--test--challenge', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-test--test--challenge" onclick="tryItOut('PUTapi-test--test--challenge');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-test--test--challenge" onclick="cancelTryOut('PUTapi-test--test--challenge');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-test--test--challenge" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/test/{test}/challenge</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>test</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="test" data-endpoint="PUTapi-test--test--challenge" data-component="url" required  hidden>
<br>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>title</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="title" data-endpoint="PUTapi-test--test--challenge" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>point</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="point" data-endpoint="PUTapi-test--test--challenge" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>description</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="description" data-endpoint="PUTapi-test--test--challenge" data-component="body"  hidden>
<br>
</p>

</form>


## api/tests/{test}/submissions




> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/tests/inventore/submissions" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/tests/inventore/submissions"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/tests/inventore/submissions'
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-GETapi-tests--test--submissions" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-tests--test--submissions"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-tests--test--submissions"></code></pre>
</div>
<div id="execution-error-GETapi-tests--test--submissions" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-tests--test--submissions"></code></pre>
</div>
<form id="form-GETapi-tests--test--submissions" data-method="GET" data-path="api/tests/{test}/submissions" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-tests--test--submissions', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-tests--test--submissions" onclick="tryItOut('GETapi-tests--test--submissions');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-tests--test--submissions" onclick="cancelTryOut('GETapi-tests--test--submissions');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-tests--test--submissions" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/tests/{test}/submissions</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>test</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="test" data-endpoint="GETapi-tests--test--submissions" data-component="url" required  hidden>
<br>
</p>
</form>


## api/tests/{test}/submissions




> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/api/tests/repellat/submissions" \
    -H "Accept: application/json" \
    -H "Content-Type: multipart/form-data" \
    -F "language=quis" \
    -F "submission_file=@/tmp/phpP1jT6o" 
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/tests/repellat/submissions"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "multipart/form-data",
};

const body = new FormData();
body.append('language', 'quis');
body.append('submission_file', document.querySelector('input[name="submission_file"]').files[0]);

fetch(url, {
    method: "POST",
    headers,
    body,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/tests/repellat/submissions'
files = {
  'submission_file': open('/tmp/phpP1jT6o', 'rb')
}
payload = {
    "language": "quis"
}
headers = {
  'Accept': 'application/json',
  'Content-Type': 'multipart/form-data'
}

response = requests.request('POST', url, headers=headers, files=files, data=payload)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-POSTapi-tests--test--submissions" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-tests--test--submissions"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-tests--test--submissions"></code></pre>
</div>
<div id="execution-error-POSTapi-tests--test--submissions" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-tests--test--submissions"></code></pre>
</div>
<form id="form-POSTapi-tests--test--submissions" data-method="POST" data-path="api/tests/{test}/submissions" data-authed="0" data-hasfiles="1" data-headers='{"Accept":"application\/json","Content-Type":"multipart\/form-data"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-tests--test--submissions', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-tests--test--submissions" onclick="tryItOut('POSTapi-tests--test--submissions');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-tests--test--submissions" onclick="cancelTryOut('POSTapi-tests--test--submissions');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-tests--test--submissions" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/tests/{test}/submissions</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>test</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="test" data-endpoint="POSTapi-tests--test--submissions" data-component="url" required  hidden>
<br>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>language</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="language" data-endpoint="POSTapi-tests--test--submissions" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>submission_file</code></b>&nbsp;&nbsp;<small>file</small>  &nbsp;
<input type="file" name="submission_file" data-endpoint="POSTapi-tests--test--submissions" data-component="body" required  hidden>
<br>
The value must be a file.</p>

</form>


## api/tests/{test}/submissions/{submission}




> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/tests/minus/submissions/qui" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/tests/minus/submissions/qui"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/tests/minus/submissions/qui'
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('GET', url, headers=headers)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-GETapi-tests--test--submissions--submission-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-tests--test--submissions--submission-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-tests--test--submissions--submission-"></code></pre>
</div>
<div id="execution-error-GETapi-tests--test--submissions--submission-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-tests--test--submissions--submission-"></code></pre>
</div>
<form id="form-GETapi-tests--test--submissions--submission-" data-method="GET" data-path="api/tests/{test}/submissions/{submission}" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-tests--test--submissions--submission-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-tests--test--submissions--submission-" onclick="tryItOut('GETapi-tests--test--submissions--submission-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-tests--test--submissions--submission-" onclick="cancelTryOut('GETapi-tests--test--submissions--submission-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-tests--test--submissions--submission-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/tests/{test}/submissions/{submission}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>test</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="test" data-endpoint="GETapi-tests--test--submissions--submission-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>submission</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="submission" data-endpoint="GETapi-tests--test--submissions--submission-" data-component="url" required  hidden>
<br>
</p>
</form>


## api/submissions/{submission}




> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/api/submissions/dolore" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/submissions/dolore"
);

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```

```python
import requests
import json

url = 'http://127.0.0.1:8000/api/submissions/dolore'
headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request('DELETE', url, headers=headers)
response.json()
```


> Example response (401):

```json
{
    "success": false,
    "code": 253,
    "locale": "en",
    "message": "Unauthenticated.",
    "data": {
        "value": "Unauthenticated."
    },
    "debug": []
}
```
<div id="execution-results-DELETEapi-submissions--submission-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-submissions--submission-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-submissions--submission-"></code></pre>
</div>
<div id="execution-error-DELETEapi-submissions--submission-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-submissions--submission-"></code></pre>
</div>
<form id="form-DELETEapi-submissions--submission-" data-method="DELETE" data-path="api/submissions/{submission}" data-authed="0" data-hasfiles="0" data-headers='{"Accept":"application\/json","Content-Type":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-submissions--submission-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-submissions--submission-" onclick="tryItOut('DELETEapi-submissions--submission-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-submissions--submission-" onclick="cancelTryOut('DELETEapi-submissions--submission-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-submissions--submission-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/submissions/{submission}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>submission</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="submission" data-endpoint="DELETEapi-submissions--submission-" data-component="url" required  hidden>
<br>
</p>
</form>



